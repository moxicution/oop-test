<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'contact'; // Is set to contact controller for now
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
