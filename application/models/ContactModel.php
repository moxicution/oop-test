<?php defined('BASEPATH') OR exit('No direct script access allowed');

class ContactModel extends MY_Model
{

    protected $id;
    protected $display_name;
    protected $name;
    protected $email;

    public function __construct()
    {
        parent::__construct();
    }

    public function view_all_contact()
    {
        $this->db->select('*')->from('contact');
        $x = $this->db->get()->result();

        return $x;
    }

    public function edit()
    {

    }

    public function create()
    {

    }

    public function update()
    {

    }

    public function delete()
    {

    }

}