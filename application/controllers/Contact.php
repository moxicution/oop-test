<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('contactmodel');
    }

    public function index()
    {
        $all_contacts = $this->contactmodel->view_all_contact();

        foreach ($all_contacts as $contact) {
            echo "Hey, Hey {$contact->name} <br />";
        }
    }
}